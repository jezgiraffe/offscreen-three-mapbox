import { useRef, useEffect } from 'react';
import mapbox from 'mapbox-gl';
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import './App.css';
import 'mapbox-gl/dist/mapbox-gl.css';

mapbox.accessToken = 'pk.eyJ1IjoiamV6enptIiwiYSI6ImNrc3Bpd2oxdzAzMGUycHBvdWgxZ3B4am4ifQ.WSGa7k2glwlfXukEIYG9dQ';

const modelOrigin = [151.2376653, -33.8335108];
const modelAltitude = 0;
const modelRotate = [Math.PI / 2, 0, 0];

const modelAsMercatorCoordinate = mapbox.MercatorCoordinate.fromLngLat(
  modelOrigin,
  modelAltitude,
);

// transformation parameters to position, rotate and scale the 3D model onto the map
const modelTransform = {
  translateX: modelAsMercatorCoordinate.x,
  translateY: modelAsMercatorCoordinate.y,
  translateZ: modelAsMercatorCoordinate.z,
  rotateX: modelRotate[0],
  rotateY: modelRotate[1],
  rotateZ: modelRotate[2],
  /* Since the 3D model is in real world meters, a scale transform needs to be
* applied since the CustomLayerInterface expects units in MercatorCoordinates.
*/
  scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits() * 100,
};

const customLayer = {
  id: '3d-model',
  type: 'custom',
  renderingMode: '3d',
  onAdd(map, gl) {
    this.camera = new THREE.Camera();
    this.scene = new THREE.Scene();

    // create two three.js lights to illuminate the model
    const directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(0, -70, 100).normalize();
    this.scene.add(directionalLight);

    const directionalLight2 = new THREE.DirectionalLight(0xffffff);
    directionalLight2.position.set(0, 70, 100).normalize();
    this.scene.add(directionalLight2);

    // use the three.js GLTF loader to add the 3D model to the three.js scene
    const loader = new GLTFLoader();
    loader.load(
      'https://docs.mapbox.com/mapbox-gl-js/assets/34M_17/34M_17.gltf',
      (gltf) => {
        this.scene.add(gltf.scene);
      },
    );
    this.map = map;

    const canvas = ('OffscreenCanvas' in window) ? (() => {
      const offscreen = document.createElement('canvas');
      offscreen.style = { width: 500, height: 500 };
      offscreen.transferControlToOffscreen();
      return offscreen;
    })() : map.getCanvas();

    this.renderer = new THREE.WebGLRenderer({
      canvas,
      context: gl,
      antialias: true,
    });

    this.renderer.autoClear = false;
  },
  render(gl, matrix) {
    const rotationX = new THREE.Matrix4().makeRotationAxis(
      new THREE.Vector3(1, 0, 0),
      modelTransform.rotateX,
    );
    const rotationY = new THREE.Matrix4().makeRotationAxis(
      new THREE.Vector3(0, 1, 0),
      modelTransform.rotateY,
    );
    const rotationZ = new THREE.Matrix4().makeRotationAxis(
      new THREE.Vector3(0, 0, 1),
      modelTransform.rotateZ,
    );

    const m = new THREE.Matrix4().fromArray(matrix);
    const l = new THREE.Matrix4()
      .makeTranslation(
        modelTransform.translateX,
        modelTransform.translateY,
        modelTransform.translateZ,
      )
      .scale(
        new THREE.Vector3(
          modelTransform.scale,
          -modelTransform.scale,
          modelTransform.scale,
        ),
      )
      .multiply(rotationX)
      .multiply(rotationY)
      .multiply(rotationZ);

    this.camera.projectionMatrix = m.multiply(l);
    this.renderer.resetState();
    this.renderer.render(this.scene, this.camera);
    this.map.triggerRepaint();
  },
};

function App() {
  const mapRef = useRef();
  const containerRef = useRef();

  useEffect(() => {
    const options = {
      container: containerRef.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [151.2376653, -33.8335108],
      zoom: 12,
      pitch: 45,
      antialias: true,
    };

    const map = new mapbox.Map(options);
    // configuration of the custom layer for a 3D model per the CustomLayerInterface

    map.on('style.load', () => {
      map.addLayer(customLayer, 'waterway-label');
    });

    mapRef.current = map;
    window.map = map;
    return () => { map.remove(); };
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <p>Hello Vite + React!</p>
        <div ref={containerRef} style={{ height: 500, width: 500, background: 'white' }} id="map" />
      </header>
    </div>
  );
}

export default App;
